use std::path::PathBuf;
use std::io::Read;

use reqwest;
use serde_derive::Deserialize;
use thiserror::Error;
use duct::cmd;
use anyhow::{anyhow, Context};
use flash::lib::FileData;
use clap::Parser;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Invalid http status: status {0}")]
    InvalidStatus(reqwest::StatusCode),
    #[error("Reqwest error: {0}")]
    ReqError(#[from] reqwest::Error),
    #[error("IO Error: {0}")]
    Ioerror(#[from] std::io::Error),
}

type Result<T> = anyhow::Result<T>;

#[derive(Deserialize, Debug)]
struct FileListData {
    id: u32,
    length: usize
}


fn make_search_request(search_term: &str, port: u32) -> Result<FileListData> {
    // Send a search request to the server
    let target_url = format!(
        "http://localhost:{}/search?query={}",
        port,
        search_term
    );

    let mut response = reqwest::get(&target_url)?.error_for_status()?;

    if response.status() != reqwest::StatusCode::OK {
        Err(Error::InvalidStatus(response.status()))?;
    }

    Ok(response.json()?)
}
fn make_list_info_request(list_id: u32, port: u32) -> Result<FileListData> {
    // Send a search request to the server
    let target_url = format!("http://localhost:{}/list?action=list_info&list_id={}", port, list_id);

    println!("target_url");

    let mut response = reqwest::get(&target_url)?.error_for_status()?;

    if response.status() != reqwest::StatusCode::OK {
        Err(Error::InvalidStatus(response.status()))?;
    }

    Ok(response.json()?)
}
fn make_filename_requests(list: &FileListData, port: u32) -> Result<Vec<FileData>> {
    let mut result = vec!();

    for i in 0..list.length {
        let target_url = format!("http://localhost:{}/list?action=get_data&list_id={}&index={}",
                                 port,
                                 list.id,
                                 i
                            );

        let mut response = reqwest::get(&target_url)?.error_for_status()?;

        if response.status() != reqwest::StatusCode::OK {
            Err(Error::InvalidStatus(response.status()))?;
        }

        let mut buffer = String::new();
        response.read_to_string(&mut buffer)
            .context("Failed to read response")?;

        result.push(serde_json::from_str(&buffer)
            .with_context(
                || format!("failed to decode FileData with json\n{}", buffer)
            )?);
    }

    Ok(result)
}


#[derive(Parser)]
struct Opt {
    #[clap(
        short,
        long,
        help="Create symlinks for all files which are found through searcing <QUERY>"
    )]
    search: Option<String>,
    #[clap(
        short, long,
        help = "Create symlinks for all files in the list with the speicified id"
    )]
    list: Option<u32>,
    #[clap(
        short,
        long,
        help = "Port to use when communicating with the server"
    )]
    port: u32,
    #[clap(
        short = 'o',
        long,
        help = "Location to create symlinks to the files"
    )]
    target_dir: PathBuf,
    #[clap(
        short = 'i',
        long,
        help = "Directory where the files are stored"
    )]
    source_dir: PathBuf,
    #[clap(
        short,
        long,
        help = "Prevent renaming the symlinks based on file dates"
    )]
    norename: bool,
}

fn main() -> Result<()> {
    let opt = Opt::from_args();

    let list_data = match (opt.search, opt.list) {
        (Some(search_term), None) =>
            make_search_request(&search_term, opt.port)
                .context("Failed to make search request")?,
        (None, Some(list_id)) =>
            make_list_info_request(list_id, opt.port)
                .context("Failed to make list request")?,
        (Some(_), Some(_)) =>
            return Err(anyhow!("You can not specify both a search and list id")),
        (None, None) =>
            return Err(anyhow!("Specify either a list id or a search query"))
    };

    let files = make_filename_requests(&list_data, opt.port)
        .context("failed to make filename requests")?;

    let mut last_date = None;
    let mut last_extra_index = 0;

    for file in files {
        let as_pathbuf = PathBuf::from(file.file_path);
        let full_source_path = opt.source_dir.join(&as_pathbuf);
        let full_target_path = if !opt.norename {
            if Some(file.creation_date) == last_date {
                last_extra_index += 1
            }
            else {
                last_date = Some(file.creation_date);
                last_extra_index = 0;
            }
            let postfix = if last_extra_index != 0 {
                format!("{}", last_extra_index)
            }
            else {
                String::new()
            };

            let extension = as_pathbuf.extension()
                .context("[internal] File had no extension")?;
            let name = format!("{}{}", file.creation_date.format("%F-%H%M%S"), postfix);
            let mut result = PathBuf::from(name);
            result.set_extension(extension);
            opt.target_dir.join(&result)
        }
        else {
            opt.target_dir.join(&as_pathbuf)
        };

        println!("{:?} -> {:?}", full_source_path, full_target_path);
        // println!("{:?}", full_source_path);

        // TODO: Maybe capture stdout when this fails
        cmd!("ln", "-s", full_source_path, full_target_path).read()
            .context("Failed to create symlink")?;
    }

    Ok(())
}
